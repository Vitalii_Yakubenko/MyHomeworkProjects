#include <iostream>
#include "Organizer.h"
#include "Contact.h"

int main()
{
    Organizer _organizer = Organizer();

    _organizer.addContact(Contact("name_is_Vitalii", "Yakubeko", "guliousss@gmail.com", "0000000000"));
    _organizer.addContact(Contact("name_is_Taras", "Rakochy", "guliousss@gmail.com", "0000000000"));
    _organizer.addContact(Contact("name_is_Ira", "Dzuba", "guliousss@gmail.com", "0000000000"));

    _organizer.deleteContact("name_is_Vitalii");

    std::cout << '\n';
    std::cout << "All contacts:" << '\n';
    _organizer.PrintAllContacts();

    std::cout << '\n';
    std::cout << '\n';

    _organizer.addNote(Note("Eleks", "some note"));
    std::cout << '\n';
    std::cout << "All notes:" << '\n';
    _organizer.PrintAllNotes();

    return 0;
}
