#ifndef NOTE_H
#define NOTE_H
#include <string>
#include <ctime>

struct Note
{
    public:
        Note(std::string highlight, std::string text);
        std::string _highlight;
        std::string _text;
        std::string time;
};

#endif // NOTE_H
