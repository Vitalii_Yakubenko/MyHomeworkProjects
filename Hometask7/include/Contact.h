#ifndef CONTACT_H_INCLUDED
#define CONTACT_H_INCLUDED
#include <string>

using namespace std;

struct Contact
{
    string Name;
    string Surname;
    string Email;
    string PhoneNumber;

    Contact(){}
    Contact(const string &name, const string &surname, const string &email, const string phoneNumber);
};

#endif // CONTACT_H_INCLUDED
