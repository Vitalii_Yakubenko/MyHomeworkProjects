#ifndef ORGANIZER_H
#define ORGANIZER_H
#include <vector>
#include "Contact.h"
#include "Note.h"

class Organizer
{
    public:
        Organizer();
        virtual ~Organizer();

        void addContact(Contact contact);
        std::vector<Contact> findContact(std::string name);
        size_t contactsCount();
        void deleteContact(std::string name);
        std::vector<Contact> getAllContacts();
        void WriteDataIntoFile(std::vector<Contact> _vector);
        void ReadDataFromFile();
        void PrintAllContacts();

        // methods for Note
        void addNote(Note note);
        std::vector<Note> findNote(std::string name);
        size_t notesCount();
        void deleteNote(std::string name);
        std::vector<Note> getAllNotes();
        void Write(std::vector<Note> note);
        void ReadNotesFromFile();
        void PrintAllNotes();

    protected:
    private:
        std::vector<Contact> contacts;
        std::vector<Note> notes;
};

#endif // ORGANIZER_H
