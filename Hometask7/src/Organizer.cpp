#include "Organizer.h"
#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <cstring>

Organizer::Organizer()
{
    ReadDataFromFile();
}

Organizer::~Organizer()
{

}

void Organizer::addContact(Contact contact)
{
    contacts.push_back(contact);
    WriteDataIntoFile(contacts);
    std::cout << "Contact " << contact.Name << " has added successfully" << '\n';
}

std::vector<Contact> Organizer::findContact(string contactName)
{
    std::vector<Contact> contactVector;
    for(auto contact : contacts){
        if(contact.Name.find(contactName) != -1)
            contactVector.push_back(contact);
    }

    std::cout << "Found contacts" << '\n';
    for(auto con : contactVector){
        std::cout << con.Name << " " << con.Surname << '\n';
    }

    return contactVector;
}

size_t Organizer::contactsCount()
{
    return contacts.size();
}

void Organizer::deleteContact(string name)
{
    size_t index = 0;
    for (auto contact : contacts){
        if(contact.Name == name)
        {
            std::cout << "Contact with name " << name << " was deleted successfully" << '\n';
            contacts.erase(contacts.begin() + index);
            WriteDataIntoFile(contacts);
            break;
        }
        ++index;
    }
}

std::vector<Contact> Organizer::getAllContacts()
{
    return contacts;
}

void Organizer::WriteDataIntoFile(std::vector<Contact> _vector)
{
    ofstream out("E:\\Hometask7\\Hometask7\\bin\\Debug\\DataFile.txt");
    for(Contact con : contacts)
    {
        out << con.Name << " " << con.Surname << " " << con.Email << " " << con.PhoneNumber << '\n';
    }
    out.close();
}

void Organizer::ReadDataFromFile()
{
    ifstream in("E:\\Hometask7\\Hometask7\\bin\\Debug\\DataFile.txt");
    while(in)
    {
        string tmpstr;
        std::getline(in, tmpstr);
        if(!tmpstr.empty())
        {
            Contact newContact;
            std::stringstream stream(tmpstr);
            stream >> newContact.Name >> newContact.Surname >> newContact.Email >> newContact.PhoneNumber;
            contacts.push_back(newContact);
        }
    }
    in.close();
}

void Organizer::PrintAllContacts()
{
    for(const Contact current : getAllContacts())
    {
        cout << current.Name << " " << current.Surname << " " <<  current.Email << " " << current.PhoneNumber << '\n';
    }
}

// methods for Note oparetions
void Organizer::addNote(Note note)
{
    time_t now = time(0);
    note.time = ctime(&now);
    notes.push_back(note);
    Write(notes);
    std::cout << "Note " << note._highlight << " has added successfully" << '\n';
}

std::vector<Note> Organizer::findNote(string noteName)
{
    std::vector<Note> noteVector;
    for(auto note : notes){
        if(note._highlight.find(noteName) != -1)
            noteVector.push_back(note);
    }

    std::cout << "Found notes" << '\n';
    for(auto note : noteVector){
        std::cout << note._highlight << " " << note._text << " " << note.time << '\n';
    }
    return noteVector;
}

size_t Organizer::notesCount()
{
    return notes.size();
}

void Organizer::deleteNote(string name)
{
    size_t index = 0;
    for (auto note : notes){
        if(note._highlight == name)
        {
            std::cout << "Note with highlight " << name << " was deleted successfully" << '\n';
            notes.erase(notes.begin() + index);
            WriteDataIntoFile(contacts);
        }
        ++index;
    }
}

std::vector<Note> Organizer::getAllNotes()
{
    return notes;
}

void Organizer::Write(std::vector<Note> _vector)
{
    ofstream out("E:\\Hometask7\\Hometask7\\bin\\Debug\\NoteDataFile.txt");
    for(auto note : _vector)
    {
        out << note._highlight << " " << note._text << " " << note.time << '\n';
    }
    out.close();
}


void Organizer::ReadNotesFromFile()
{

}

void Organizer::PrintAllNotes()
{
    for(const Note current : getAllNotes())
        {
            cout << current._highlight << " " << current._text << " " <<  current.time << '\n';
        }
}
