﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library
{
    internal class Library : CountingBooksBase
    {
        private readonly List<Department> _departments; // перенести це оголошення в конструктор. + забрати із public Library() : this(String.Empty)- String.Empty, краще заносити "Unknown" чи ще щось.

        public string Name { get; set; }

        public Library() : this("Unknown") { }

        public Library(string Name)
        {
            this.Name = Name;
            _departments = new List<Department>();
        }

        public void AddDepartment(Department department)
        {
            var d = PeekDepartment(department.Name);
            if (d != null)
                Console.WriteLine("Department with the same name has already added.");
            else
            {
                _departments.Add(department);
            }
        }

        private Department PeekDepartment(string name)
        {
            var conteiner = _departments.Find(n => n.Name == name);
            if (conteiner != null) return conteiner;
            else return null;
        }

        public List<Department> GetDepartments()
        {
            return _departments;
        }

        public override int BooksCount()
        {
            int count = 0;
            foreach(var department in _departments)
            {
                count += department.BooksCount();
            }
            return count;
        }
        public override string ToString()
        {
            string str = null;
            foreach(var dep in _departments)
            {
                str += dep.Name + "\n";
            }
            return str;
        }

        public Author authorWithMaxBookCount(List<Author> authorList)
        {
            Author resAuthor = authorList.OrderBy(n => n.BooksCount()).Last();
            return resAuthor;
        }

        public Department DepartmentWithMaxBooksNum()
        {
            Department tmp = _departments.OrderByDescending(n => n.BooksCount()).First();
            return tmp;
        }

        public Book BookWithMinCount()
        {
            var book = _departments.OrderBy(n => n.BookWithMinCount()).First();
            
            return book.BookWithMinCount();
        }
    }
}
