﻿namespace Library
{
    internal interface ICountingBooks
    {
        int BooksCount();
    }
}
