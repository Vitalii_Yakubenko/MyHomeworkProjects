﻿using System;

namespace Library
{
    internal abstract class CountingBooksBase : ICountingBooks, IComparable
    {
        public virtual int BooksCount()
        {
            return 0;
        }

        public int CompareTo(object obj)
        {
            var d = obj as CountingBooksBase;
            if (d == null)
                return -1;

            var count1 = BooksCount();
            var count2 = d.BooksCount();
            return count1 > count2
                ? 1
                : (count1 == count2 ? 0 : -1);
        }
    }
}
