﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Library
{
    internal class Program
    {
        public static List<Author> authorList;
        public static Library library;
        private static void Main(string[] args)
        {
            authorList = new List<Author> {new Author("Vitalii"), new Author("Taras"), new Author("Tania") };
            library = new Library("MyBookLibrary");

            Department IT = new Department("IT department");
            Department Foreigh = new Department("Foreigh literature department");
            Department Ukrainian = new Department("Ukrainian litareure department");
            Department Science = new Department("Ukrainian litareure department");
            library.AddDepartment(IT);
            library.AddDepartment(Foreigh);
            library.AddDepartment(Ukrainian);
            library.AddDepartment(Science);
            
            Book spartak = new Book("Spartak", 300, (authorList.Where(author => author.Name == "Vitalii") as Author));
            Book fox = new Book("Fox", 100, (authorList.Where(author => author.Name == "Tania") as Author));
            Book witcher = new Book("Witcher", 1000, (authorList.Where(author => author.Name == "Taras") as Author));
            
            authorList[0].AddPublication(spartak);
            authorList[2].AddPublication(fox);
            authorList[1].AddPublication(witcher);

            Ukrainian.AddBook(fox);
            Foreigh.AddBook(spartak);
            Foreigh.AddBook(witcher);

            Author newAuthor = new Author("Oleg");
            authorList.Add(newAuthor);

            
            
            XmlLibrary.WriteXml(library, "HomeWorkXMLLibrary.xml");
            //XmlLibrary.Change("HomeWorkXMLLibrary.xml");
            //library = XmlLibrary.ReadXml("HomeWorkXMLLibrary.xml", out authorList);

            // Тестові виводи на екран
            Console.WriteLine($"The book with min count of page in library - {library.BookWithMinCount()}");
            Console.WriteLine('\n' + $"The author with max count of books - {library.authorWithMaxBookCount(authorList).Name}");
            Console.WriteLine('\n' + $"The department with max count of books - {library.DepartmentWithMaxBooksNum().Name}");
            Console.ReadKey();
        }
    }
}
