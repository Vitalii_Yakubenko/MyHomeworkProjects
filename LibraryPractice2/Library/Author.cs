﻿using System;
using System.Collections.Generic;

namespace Library
{
    internal class Author : CountingBooksBase
    {
        private readonly List<Book> _publications;

        public string Name { get; set; }
        //public int Age { get; set; }

        public Author() : this("Unknown") { }

        public Author(string name)
        {
            Name = name;
            //Age = age;
            _publications = new List<Book>();
        }

        public void AddPublication(Book publication)
        {
            var p = PeekPublication(publication.Name);
            if (p == null)
            {
                publication.Author = this;
                _publications.Add(publication);
            }
        }

        public List<Book> GetBooks() => _publications;
        
        public Book RemovePublication(string name)
        {
            return PeekPublication(name);
        }

        private Book PeekPublication(string name)
        {
            foreach (var publication in _publications)
                if (publication.Name == name)
                    return publication;
            return null;
        }

        public override int BooksCount()
        {
            return _publications.Count;
        }

        public override string ToString()
        {
            return $"{Name}: has published {BooksCount()} books";
        }
    }
}
