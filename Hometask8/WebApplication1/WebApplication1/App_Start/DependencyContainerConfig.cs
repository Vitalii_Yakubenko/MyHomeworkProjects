﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using WebApplication1.Controllers;
using WebApplication1.Interfaces;
using WebApplication1.Models;

namespace WebApplication1.App_Start
{
    public class DependencyContainerConfig
    {
        public static void Register (HttpConfiguration config)
        {
            var container = new UnityContainer();
            container.RegisterType<IDataManager, StudentDataManager>(new HierarchicalLifetimeManager());
            container.RegisterType<IStudent, Student>(new HierarchicalLifetimeManager());
            config.DependencyResolver = new UnityResolver(container);
            //var controller = container.Resolve<StudentsController>();
        }
    }
}