﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Interfaces;

namespace WebApplication1.Models
{
    public class Student : IStudent
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Surname {get; set;}
        public int Course { get; set; }
    }
}