﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApplication1.Interfaces
{
    public interface IStudent
    {
        int ID { get; set; }
        string Name { get; set; }
        string Surname { get; set; }
        int Course { get; set; }
    }
}
