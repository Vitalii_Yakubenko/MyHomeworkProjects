﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using WebApplication1.Models;

namespace WebApplication1.Interfaces
{
    public interface IDataManager
    {
        IEnumerable<IStudent> GetAllStudents();
        void Post(IStudent student);
        IHttpActionResult Put(IStudent student);
        void Delete(int id);
    }
}
