﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using WebApplication1.Interfaces;
using WebApplication1.Models;

namespace WebApplication1
{
    public class StudentDataManager : IDataManager
    {
        private List<IStudent> studentslist;

        public StudentDataManager()
        {
            studentslist = new List<IStudent>();
            studentslist.Add(new Student { ID = 1, Name = "Vitalik", Surname = "Yakubenko", Course = 5 });
        }

        public void Delete(int id)
        {
            var student = studentslist.Find(x => x.ID == id);
            studentslist.Remove(student);
        }

        public IEnumerable<IStudent> GetAllStudents()
        {
            return studentslist;
        }

        public void Post(IStudent student)
        {
            studentslist.Add(student);
        }

        public IHttpActionResult Put(IStudent student)
        {
            throw new NotImplementedException();
        }


    }
}