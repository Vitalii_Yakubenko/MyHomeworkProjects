﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication1.Interfaces;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class StudentsController : ApiController
    {
        private readonly IDataManager _dataManager;

        public StudentsController(IDataManager dataManager)
        {
            _dataManager = dataManager;
        }

        // GET: api/Students
        public IEnumerable<IStudent> Get()
        {
            return _dataManager.GetAllStudents();
        }

        // POST: aps/Students
        public HttpResponseMessage Post([FromBody]Student student)
        {
            if(student == null)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
            
            if(_dataManager.GetAllStudents().FirstOrDefault(x => x.ID == student.ID) != null)
            {
                var response = new HttpResponseMessage
                {
                    Content = new StringContent("There is student with such id")
                };
                return response;
            }

            _dataManager.Post(student);
            return new HttpResponseMessage(HttpStatusCode.Created);
        }

        public IHttpActionResult Put([FromBody]Student student)
        {
            if (student.ID < 0 || student.ID >= _dataManager.GetAllStudents().Count())
            {
                return BadRequest();
            }
            var currentStudent = _dataManager.GetAllStudents().FirstOrDefault(x => x.ID == student.ID);
            if (currentStudent == null)
            {
                return InternalServerError();
            }
            currentStudent.Name = student.Name;
            return Ok();
        }

        // DELETE: api/Students/5
        public IHttpActionResult Delete(int id)
        {
            _dataManager.Delete(id);
            return Ok();
        }
    }
}