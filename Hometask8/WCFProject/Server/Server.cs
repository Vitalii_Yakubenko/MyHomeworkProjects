﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    class Server
    {
        static void Main(string[] args)
        {
            Console.Title = "Server";

            Uri address = new Uri("http://localhost:5005/IContract");

            BasicHttpBinding binding = new BasicHttpBinding();

            Type contract = typeof(IContract);

            ServiceHost host = new ServiceHost(typeof(Service));

            host.AddServiceEndpoint(contract, binding, address);

            host.Open();

            Console.WriteLine("Server is running");
            Console.ReadKey();

            host.Close();
        }
    }
}
