﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace WCFProject
{
    [ServiceContract]
    interface IContract
    {
        [OperationContract]
        string Send(string input);
    }
}
