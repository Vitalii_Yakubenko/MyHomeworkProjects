﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;

namespace WCFProject
{
    class Client
    {
        static void Main(string[] args)
        {
            Uri UriAddress = new Uri("http://localhost:5005/IContract");

            BasicHttpBinding binding = new BasicHttpBinding();

            EndpointAddress endpoint = new EndpointAddress(UriAddress);

            ChannelFactory<IContract> factory = new ChannelFactory<IContract>(binding, endpoint);

            IContract channel = factory.CreateChannel();

            Console.WriteLine("Type message to start working with server");
            var message = Console.ReadLine();

            while (message != "Exit")
            {
                var response = channel.Send(message);
                Console.WriteLine(response + Environment.NewLine);
                message = Console.ReadLine();
            }
        }
    }
}
