﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task4
{
    class Program
    {
        static void Main(string[] args)
        {
            int size = 8;
            int[,] matrix = new int[size, size];
            InitialMatrix(ref matrix);
            /*Random rnd = new Random();
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    matrix[i, j] = rnd.Next(-1000, 1001);
                }
            }*/
            // Show matrix
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    Console.Write(matrix[i, j] + "\t");
                }
                Console.Write("\n");
            }

            int[] respart1 = part1Task4(matrix, size);
            Console.Write("Номери K:");
            for (int i = 0; i < respart1.Length; i++)
            {
                Console.Write(respart1[i] + "\t");
            }
            Console.WriteLine();

            part2Task4(matrix, size);
            
            Console.ReadKey();
        }

        public static void InitialMatrix(ref int[,] matrix)
        {
            int[,] newMatrix = new int[8, 8] { { 5, 4, 4, 3, 2, 10, 9, 8 },
                                               { 1, 2, 3, 4, 1, 6, 7, 8 },
                                                {2, 1, 3, 2, 1, 2, 3 , 8},
                                                {2, 1, 3, 2, 1, 2, 3 , 8},
                                                { 5, 4, 3, 2, 1, 10, 9, 8},
                                                {2, 1, 3, 2, -1, 2, 3 , 8 },
                                                { 1, 2, 3, -4, 1, 6, 7, 8},
                                                { 1, 2, 3, -4, 1, 6, 7, 8},
            };
            matrix = newMatrix;
        }

        public static int[] part1Task4(int[,] matrix, int size)
        {
            int[] resK = new int[0];
            
            for(int k = 0; k < size; k++)
            {
                int i = 0, j = 0;
                bool flag = true;
                while (flag && i < size)
                {
                    if (matrix[i, k] == matrix[k, j])
                    {
                        flag = true;
                        ++i;
                        ++j;
                    }
                    else
                    {
                        flag = false;
                    }
                }
                if (flag)
                {
                    AddNum(ref resK, k);
                }
            }
            return resK;
        }

        public static void AddNum(ref int[] resarr, int k)
        {
            int[] resbmp = new int[resarr.Length + 1];
            int i = 0;
            for (; i < resarr.Length; i++)
            {
                resbmp[i] = resarr[i];
            }
            resbmp[i] = k;
            resarr = resbmp;
        }

        public static void part2Task4(int[,] matrix, int size)
        {
            int index = -1;
            int[] tmparr = new int[size];
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    tmparr[j] = matrix[i, j];

                }
                if (HasNegativeNumber(tmparr))
                {
                    int sum = 0;
                    for (int j = 0; j < size; j++)
                    {
                        sum += matrix[i, j];
                    }
                    Console.WriteLine($"Сума чисел в рядку {i} = {sum}");
                }
            }
        }


        public static bool HasNegativeNumber(int[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] < 0)
                    return true;
            }
            return false;
        }

    }
}
