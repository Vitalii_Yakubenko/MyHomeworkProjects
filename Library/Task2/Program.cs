﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2
{
    class Program
    {
        static void Main(string[] args)
        {
            int matrixRow = 5, matrixColumn = 10;
            int[,] matrix = new int[matrixRow, matrixColumn];
            Random rnd = new Random();
            for(int i = 0; i < matrixRow; i++)
            {
                for(int j = 0; j < matrixColumn; j++)
                {
                    matrix[i, j] = rnd.Next(0, 11);
                }
            }
            // Show matrix
            for (int i = 0; i < matrixRow; i++)
            {
                for (int j = 0; j < matrixColumn; j++)
                {
                    Console.Write(matrix[i, j] + "\t");
                }
                Console.Write("\n");
            }

            int index = part1Task2(matrix, matrixRow, matrixColumn);
            if (index == -1) Console.WriteLine("В матрицi нема стовпця без вiд'ємних чисел");

            int[,] modifyMatrix = part2Task2(matrix, matrixRow, matrixColumn);

            for (int i = 0; i < matrixRow; i++)
            {
                for (int j = 0; j < matrixColumn; j++)
                {
                    Console.Write(modifyMatrix[i, j] + "\t");
                }
                Console.Write("\n");
            }

            Console.ReadKey();
        }

        public static int part1Task2(int[,] matrix, int matrixRow, int matrixColumn)
        {
            int index = -1;
            for (int j = 0; j < matrixColumn; j++) 
            {
                for (int i = 0; i < matrixRow; i++)
                {
                    if (matrix[i, j] >= 0)
                    {
                        index = j;
                    }
                    else
                    {
                        index = -1;
                        break;
                    }
                }
                if(index > -1)
                {
                    Console.WriteLine("Номер стовпця - " + (index + 1));
                    return index + 1;
                }
            }
            return -1;
        }

        public static int[,] part2Task2(int[,] matrix, int matrixRow, int matrixColumn)
        {
            int[,] map = new int[matrixRow, 2];
            int[,] modifyMatrix = new int[0, matrixColumn];

            for (int i = 0; i < matrixRow; i++)
            {
                int[] arr = new int[matrixColumn];
                for (int j = 0; j < matrixColumn; j++)
                {
                    arr[j] = matrix[i, j];
                }
                Sort(ref arr);
                int jj = 0;
                for (int ii = 1; ii < arr.Length; ii++)
                {
                    if (arr[ii] > arr[ii - 1])
                        jj++;
                    else
                        continue;
                }
                map[i, 0] = i;
                map[i, 1] = jj;
            }
            
            /*for(int i = 0; i < matrixRow; i++)
            {
                for(int j = 0; j < 2; j++)
                {
                    Console.Write(map[i, j] + "\t");
                }
                Console.WriteLine();
            }
            Console.WriteLine();*/

            for(int k = 0; k < matrixRow; k++)
            {
                int tmpNumForCompare = 0, indexNumForCompare = 0;
                int i = 0;
                for (; i < matrixRow; i++)
                {
                    if (map[i, 1] > tmpNumForCompare)
                    {
                        tmpNumForCompare = map[i, 1];
                        indexNumForCompare = map[i, 0];
                    }
                }
                AddRowToArray(matrix, ref modifyMatrix, indexNumForCompare, matrixRow, matrixColumn);
                map[indexNumForCompare, 1] = -1;
            }
            return modifyMatrix;
        }

        public static void AddRowToArray(int[,] matrix, ref int[,] modifymatrix, int index, int row, int column)
        {
            int[,] tmpArray = new int[modifymatrix.Length / 10 + 1, column];
            int i = 0;
            for (; i < modifymatrix.Length / 10; i++)
            {
                for(int j = 0; j < column; j++)
                {
                    tmpArray[i, j] = modifymatrix[i, j];
                }
            }
            for(int j = 0; j < column; j++)
            {
                tmpArray[i, j] = matrix[index, j];
            }

            modifymatrix = tmpArray;
        }

        public static void Sort(ref int[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                int k = i;
                for (int j = i + 1; j < arr.Length; j++)
                    if (arr[k] > arr[j])
                        k = j;
                int temp = arr[k];
                arr[k] = arr[i];
                arr[i] = temp;
            }
        }
    }
}
