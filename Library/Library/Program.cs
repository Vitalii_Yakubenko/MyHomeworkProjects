﻿using System;

namespace Library
{
    internal class Program
    {
        public static Author[] authorArray;

        private static void Main(string[] args)
        {
            authorArray = new Author[] {new Author("Vitalii", 21), new Author("Taras", 22), new Author("Tania", 20) };
            var library = new Library();

            Department IT = new Department("IT department");
            Department Foreigh = new Department("Foreigh literature department");
            Department Ukrainian = new Department("Ukrainian litareure department");
            library.AddDepartment(IT);
            library.AddDepartment(Foreigh);
            library.AddDepartment(Ukrainian);

            Book spartak = new Book("Spartak", 300, authorArray[0]);
            Book fox = new Book("Fox", 100, authorArray[2]);
            Book witcher = new Book("Witcher", 1000, authorArray[1]);
            authorArray[0].AddPublication(spartak);
            authorArray[0].AddPublication(fox);
            authorArray[2].AddPublication(fox);
            authorArray[1].AddPublication(witcher);

            Ukrainian.AddBook(fox);
            Foreigh.AddBook(spartak);
            Foreigh.AddBook(witcher);

            Author newAuthor = new Author("Oleg", 20);
            AddAuthor(newAuthor);
            
            
            // Тестові виводи на екран
            //Console.WriteLine(authorArray[0].ToString());
            //Console.WriteLine(Ukrainian.ToString());
            /*foreach(var i in authorArray)
            {
                Console.WriteLine(i.Name);
            }*/
            //Console.WriteLine($"The book with min count of page in library - {library.BookWithMinCount()}");

            Console.ReadKey();
        }

        public static void AddAuthor(Author author)
        {
            Author[] tmp = new Author[authorArray.Length + 1];
            int tmpI = 0;
            for (int i = 0; i < authorArray.Length; i++, tmpI++)
            {
                tmp[i] = authorArray[i];
            }
            tmp[tmpI] = author;
            authorArray = tmp;
        }
    }
}
