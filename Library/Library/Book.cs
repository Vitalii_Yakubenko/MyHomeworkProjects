﻿using System;

namespace Library
{
    internal class Book : IComparable
    {
        public string Name { get; set; }
        public int PagesCount { get; set; }
        public Author Author { get; set; }

        public Book() : this("Unknown", 0, null) { }

        public Book(string name, int pagesCount, Author author)
        {
            Name = name;
            PagesCount = pagesCount;
            Author = author;
        }

        public int CompareTo(object obj)
        {
            var d = obj as Book;
            return d == null || PagesCount > d.PagesCount
                ? 1
                : (PagesCount == d.PagesCount ? 0 : -1);
        }
        public override string ToString()
        {
            return Name;
        }
    }
}
