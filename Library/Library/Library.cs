﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Library
{
    internal class Library : CountingBooksBase
    {
        private readonly List<Department> _departments; // перенести це оголошення в конструктор. + забрати із public Library() : this(String.Empty)- String.Empty, краще заносити "Unknown" чи ще щось.

        public string Name { get; set; }

        public Library() : this("Unknown") { }

        public Library(string Name)
        {
            this.Name = Name;
            _departments = new List<Department>();
        }

        public void AddDepartment(Department department)
        {
            var d = PeekDepartment(department.Name);
            if (d != null)
                Console.WriteLine("Department with the sane name has already added.");
            else
            {
                _departments.Add(department);
            }
        }

        private Department PeekDepartment(string name)
        {
            foreach(var container in _departments)
            {
                if (container.Name == name)
                    return container;
            }
            return null;
        }
        public override int BooksCount()
        {
            int count = 0;
            foreach(var department in _departments)
            {
                count += department.BooksCount();
            }
            return count;
        }
        public override string ToString()
        {
            string str = null;
            foreach(var dep in _departments)
            {
                str += dep.Name + "\n";
            }
            return str;
        }

        public Author authorWithMaxBookCount(Author[] authorArray)
        {
            Author tmpNum = authorArray[0];
            int index = 0;
            for(int i = 0; i < authorArray.Length-1; i++)
            {
                if(authorArray[i].CompareTo(tmpNum) == 1) {
                    tmpNum = authorArray[i];
                    index = i;
                }
            }
            return authorArray[index];
        }

        public Department DepartmentWithMaxBooksNum()
        {
            Department tmp = _departments[0];
            foreach(var dep in _departments)
            {
                if(dep.CompareTo(tmp) == 1)
                {
                    tmp = dep;
                }
            }
            return tmp;
        }

        public Book BookWithMinCount()
        {
            var tmpBook = _departments[0].BookWithMinCount();
            foreach (var department in _departments)
            {
                if (department.BookWithMinCount().CompareTo(tmpBook) == -1)
                {
                    tmpBook = department.BookWithMinCount();
                }
            }
            return tmpBook;
        }
    }
}
