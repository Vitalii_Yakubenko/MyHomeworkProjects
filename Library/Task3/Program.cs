﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3
{
    class Program
    {
        static void Main(string[] args)
        {
            int matrixRow = 5, matrixColumn = 10;
            int[,] matrix = new int[matrixRow, matrixColumn];
            InitialMatrix(ref matrix);
            /*Random rnd = new Random();
            for (int i = 0; i < matrixRow; i++)
            {
                for (int j = 0; j < matrixColumn; j++)
                {
                    matrix[i, j] = rnd.Next(-1000, 1001);
                }
            }*/
            // Show matrix
            for (int i = 0; i < matrixRow; i++)
            {
                for (int j = 0; j < matrixColumn; j++)
                {
                    Console.Write(matrix[i, j] + "\t");
                }
                Console.Write("\n");
            }

            part1Task3(matrix, matrixRow, matrixColumn);
            
            int[,] resPart2 = part2Task3(matrix, matrixRow, matrixColumn);
            
            for (int i = 0; i < resPart2.Length / 2; i++)
            {
                Console.WriteLine($"Координати сiдловоi точки: x = {resPart2[i, 0]}, y = {resPart2[i, 1]}");
            }
            Console.ReadKey();
        }

        public static void InitialMatrix(ref int[,] matrix)
        {
            int[,] newMatrix = new int[5, 10] { { 5, 4, 4, 3, -2, 10, 9, 8, 7, 6 },
                                               { 1, 2, 3, -4, 1, 6, 7, 8, 9, 10 },
                                                {2, 1, 3, -2, 1, 2, 3 , 8, 9, 10 },
                                                {2, 1, 3, 2, 1, 2, 3 , 8, 9, 10 },
                                                { 5, 4, 3, 2, 1, 10, 9, 8, 7, 6 },
            };
            matrix = newMatrix;
        }

        public static void part1Task3(int[,] matrix, int matrixRow, int matrixColumn)
        {
            int index = -1;
            int[] tmparr = new int[matrixColumn];
            for (int i = 0; i < matrixRow; i++)
            {
                for (int j = 0; j < matrixColumn; j++)
                {
                    tmparr[j] = matrix[i, j];

                }
                if (HasNegativeNumber(tmparr))
                {
                    int sum = 0;
                    for (int j = 0; j < matrixColumn; j++)
                    {
                        sum += matrix[i, j];
                    }
                    Console.WriteLine($"Сума чисел в рядку {i} = {sum}");
                }
            }
        }

        public static bool HasNegativeNumber(int[] array)
        {
            for(int i = 0; i < array.Length; i++)
            {
                if (array[i] < 0)
                    return true;
            }
            return false;
        }
        
        public static int[,] part2Task3(int[,] matrix, int matrixRow, int matrixColumn)
        {
            int flag = -1;
            int[,] resMap = new int[0, 2];
            for(int i = 0; i < matrixRow; i++)
            {
                int row = 0, column = 0;
                int element = matrix[i, 0];
                for (int j = 0; j < matrixColumn; j++)
                {
                    if(matrix[i, j] < element)
                    {
                        element = matrix[i, j];
                        row = i;
                        column = j;
                    }
                }
                for(int k = 0; k < matrixRow; k++)
                {
                    if (matrix[k, column] > element)
                    {
                        flag = -1;
                        break;
                    }  
                    else
                    {
                        flag = 1;
                    }
                }
                if(flag == 1)
                {
                    AddNum(ref resMap, row, column);
                    //resMap[i, 0] = row;
                    //resMap[i, 1] = column;
                }
            }
            return resMap;
        }

        public static void AddNum(ref int[,] resMap, int row, int column)
        {
            int[,] restmp = new int[resMap.Length / 2 + 1, 2];
            int i = 0, j = 0;
            for (; i < resMap.Length / 2; i++)
            {
                for(; j < 2; j++)
                {
                    restmp[i, j] = resMap[i, j];
                }
            }
            restmp[i, 0] = row;
            restmp[i, 1] = column;
            resMap = restmp;
        }

    }
}
