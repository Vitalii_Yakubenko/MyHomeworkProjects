﻿using System;
using System.Collections.Generic;
using System.Linq;
using Hometask4.Interfaces;
using Newtonsoft.Json;
using System.IO;

namespace Hometask4
{
    internal class JsonStorage : IStorage
    {
        private const string FileName = "Json.json";

        private List<User> usersList;

        private List<User> Users
        {
            get { return usersList ?? (usersList = ReadFromJson()); }
        }

        public void Add(User user)
        {
            var exist = Users.FirstOrDefault(n => n.userID == user.userID);
            if (exist == null)
            {
                Users.Add(user);
                WriteToJson();
            }
            else { Console.WriteLine("User has been already added. Please change user id"); }
        }

        public void Edit(User user)
        {
            var exist = Users.FirstOrDefault(n => n.userID == user.userID);
            if (exist != null)
            {
                Users.Remove(exist);
                Users.Add(user);
                WriteToJson();
            }
        }

        public User GetUser(int id)
        {
            return Users.FirstOrDefault(n => n.userID == id);
        }

        public IEnumerable<User> GetUsers()
        {
            return Users;
        }

        public void Remove(int id)
        {
            var exist = Users.FirstOrDefault(n => n.userID == id);
            if (exist != null)
            {
                Users.Remove(exist);
                WriteToJson();
            }
        }

        private List<User> ReadFromJson()
        {
            List<User> startList = new List<User>();
            try
            {
                using (StreamReader sr = new StreamReader(FileName))
                {
                    string line = sr.ReadLine();
                    if (!String.IsNullOrEmpty(line))
                    {
                        startList = JsonConvert.DeserializeObject<List<User>>(line);
                    }
                }
            }
            catch (FileNotFoundException) { }

            return startList;
        }

        private void WriteToJson()
        {
            var json = JsonConvert.SerializeObject(Users);
            using (StreamWriter sw = new StreamWriter(FileName, false))
            {
                sw.WriteLine(json);
            }
        }
    }
}
