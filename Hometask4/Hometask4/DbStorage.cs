﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hometask4.Interfaces;
using System.Data.Entity;

namespace Hometask4
{
    class DbStorage : IStorage
    {
        public void Add(User user)
        {
            using (UserContext db = new UserContext())
            {
                db.Users.Add(user);
                db.SaveChanges();
            }
        }

        public void Edit(User user)
        {
            using(UserContext db = new UserContext())
            {
                var exist = db.Users.FirstOrDefault(n => n.userID == user.userID);
                if(exist != null)
                {
                    db.Users.FirstOrDefault(n => n.userID == user.userID).Name = user.Name;
                    db.SaveChanges();
                }
                
            }
        }

        public User GetUser(int id)
        {
            User currentUser;
            using (UserContext db = new UserContext())
            {
                currentUser = db.Users.FirstOrDefault(n => n.userID == id);
            }
           return currentUser;
        }

        public IEnumerable<User> GetUsers()
        {
            using (UserContext db = new UserContext())
            {
                return db.Users;
            }     
        }

        public void Remove(int id)
        {
            using(UserContext db = new UserContext())
            {
                var user = db.Users.FirstOrDefault(n => n.userID == id);
                if(user != null)
                {
                    db.Users.Remove(user);
                    db.SaveChanges();
                }
            }
        }
    }
}
