﻿using Hometask4.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hometask4
{
    class UpdateCommand : ICommand
    {
        private IStorage _storage;
        string[] _userObj;
        public UpdateCommand(string[] userObj, IStorage storage)
        {
            _userObj = userObj;
            _storage = storage;
        }

        public void Execute()
        {
            var currentUser = _storage.GetUser(Convert.ToInt32(_userObj[0]));
            currentUser.Name = _userObj[1];
            _storage.Edit(currentUser);
        }

        public void Undo()
        {
            throw new NotImplementedException();
        }
    }
}
