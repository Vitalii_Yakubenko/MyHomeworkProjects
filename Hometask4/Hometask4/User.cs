﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hometask4
{
    class User
    {
        public int userID { get; set; }
        public string Name { get; set; }
        public string lastName { get; set; }

        public User() { }

        public User(int id, string name, string lname)
        {
            userID = id;
            Name = name;
            lastName = lname;
        }
    }
}
