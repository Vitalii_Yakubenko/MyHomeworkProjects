﻿using Hometask4.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hometask4
{
    class Program
    {
        static void Main(string[] args)
        {
            var storageProvider = new StorageProvider();    
            Handler Add = new AddHandler(storageProvider);
            Handler Delete = new DeleteHandler(storageProvider);
            Handler Update = new UpdateHandler(storageProvider);
            Handler Get = new GetHandler(storageProvider);
            Handler Set = new SetStorageHandler(storageProvider);
            Handler Default = new DefaultHandler(storageProvider);
            
            Console.WriteLine("Welcome to Console. First of all enter storage device (json or db).\n Example:\n     set db\n     add 1 Vitalii Yakubenko\n     delete 1");

            while (true)
            {
                Add.Successor = Delete;
                Delete.Successor = Update;
                Update.Successor = Get;
                Get.Successor = Set;
                Set.Successor = Default;

                Console.Write(">>> ");
                string command = Console.ReadLine();

                // These two lines is my own interpreter
                string command_main = command.Split(new char[] { ' ' }).First();
                string[] arguments = command.Split(new char[] { ' ' }).Skip(1).ToArray();
                
                Add.HandlerRequest(command_main, arguments);
            }
        }

        /*private static Dictionary<string, Action<string[]>> lCommands =
            new Dictionary<string, Action<string[]>>()
            {
                { "help", HelpFunc },
                { "cp" , CopyFunc }
            };*/

        /*private static void CopyFunc(string[] obj)
        {
            if (obj.Length != 2) return;
            Console.WriteLine("Copying " + obj[0] + " to " + obj[1]);
        }*/

        /*public static void HelpFunc(string[] args)
        {
            Console.WriteLine("===== SOME MEANINGFULL HELP ==== ");
        }*/
    }
}
