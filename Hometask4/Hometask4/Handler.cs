﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hometask4.Interfaces;

namespace Hometask4
{
    abstract class Handler
    {
        protected StorageProvider StorageProvider { get; set; }
        public Handler Successor { get; set; }
        public abstract void HandlerRequest(string command, string[] obj);

        protected Handler(StorageProvider storageProvider)
        {
            StorageProvider = storageProvider;
        }
    }
}
