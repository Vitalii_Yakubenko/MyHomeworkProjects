﻿using Hometask4.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hometask4
{
    class UpdateHandler : Handler
    {
        public override void HandlerRequest(string command, string[] obj)
        {
            if (command == "update")
            {
                ICommand com = new UpdateCommand(obj, StorageProvider.Storage);
                com.Execute();
                Console.WriteLine("User has been update.");
            }
            else if (Successor != null)
            {
                Successor.HandlerRequest(command, obj);
            }
        }

        public UpdateHandler(StorageProvider storageProvider) : base(storageProvider)
        {
        }
    }
}
