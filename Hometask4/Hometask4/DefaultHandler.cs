﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hometask4
{
    class DefaultHandler : Handler
    {
        public override void HandlerRequest(string command, string[] obj)
        {
            Console.WriteLine($"Command {command} not found.");
        }
        public DefaultHandler(StorageProvider storageProvider) : base(storageProvider)
        {
        }
    }
}
