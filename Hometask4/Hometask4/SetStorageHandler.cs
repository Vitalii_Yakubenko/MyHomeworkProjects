﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hometask4
{
    class SetStorageHandler : Handler
    {
        public override void HandlerRequest(string command, string[] obj)
        {
            if (command == "set")
            {
                switch (obj[0])
                {
                    case "json":
                        StorageProvider.Storage = new JsonStorage();
                        Console.WriteLine("User has setted json.");
                        break;
                    case "db":
                        StorageProvider.Storage = new DbStorage();
                        Console.WriteLine("User has setted db.");
                        break;
                }
                
            }
            else if (Successor != null)
            {
                Successor.HandlerRequest(command, obj);
            }
        }

        public SetStorageHandler(StorageProvider storageProvider) : base(storageProvider)
        {
        }

    }
}
