﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hometask4.Interfaces
{
    interface IStorage
    {
        void Add(User user);
        void Edit(User user);
        void Remove(int id);
        User GetUser(int id);
        IEnumerable<User> GetUsers();
    }
}
