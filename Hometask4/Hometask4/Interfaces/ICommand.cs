﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hometask4.Interfaces
{
    interface ICommand
    {
        void Execute();
        void Undo();
    }
}
