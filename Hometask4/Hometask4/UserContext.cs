﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hometask4
{
    class UserContext : DbContext 
    {
        public UserContext() : base("database") { }
        public DbSet<User> Users { get; set; }
    }
}
