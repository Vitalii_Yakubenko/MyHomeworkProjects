﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hometask4.Interfaces;

namespace Hometask4
{
    class DeleteHandler : Handler
    {
        public override void HandlerRequest(string command, string[] obj)
        {
            if (command == "delete")
            {
                ICommand com = new DeleteCommand(Convert.ToInt32(obj[0]), StorageProvider.Storage);
                com.Execute();
                Console.WriteLine("User has been deleted.");
            }
            else if (Successor != null)
            {
                Successor.HandlerRequest(command, obj);
            }
        }

        public DeleteHandler(StorageProvider storageProvider) : base(storageProvider)
        {
        }
    }
}
