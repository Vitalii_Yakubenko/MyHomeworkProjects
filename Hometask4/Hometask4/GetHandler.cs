﻿using Hometask4.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hometask4
{
    class GetHandler : Handler
    {
        public override void HandlerRequest(string command, string[] obj)
        {
            if (command == "get")
            {
                ICommand com = new GetCommand(Convert.ToInt32(obj[0]), StorageProvider.Storage);
                com.Execute();
            }
            else if (Successor != null)
            {
                Successor.HandlerRequest(command, obj);
            }
        }

        public GetHandler(StorageProvider storageProvider) : base(storageProvider)
        {
        }
    }
}
