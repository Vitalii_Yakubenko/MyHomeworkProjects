﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hometask4.Interfaces;

namespace Hometask4
{
    class AddCommand : ICommand
    {
        private User _user;
        private IStorage _storage;
        public AddCommand(string[] userObj, IStorage storage)
        {
            _user = new User(Convert.ToInt32(userObj[0]), userObj[1], userObj[2]);
            _storage = storage;
        } 

        public void Execute()
        {
            _storage.Add(_user);
        }

        public void Undo()
        {
            throw new NotImplementedException();
        }
    }
}
