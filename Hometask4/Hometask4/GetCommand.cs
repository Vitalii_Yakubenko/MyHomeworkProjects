﻿using Hometask4.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hometask4
{
    class GetCommand : ICommand
    {
        private int _userID;
        private IStorage _storage;
        public GetCommand(int userID, IStorage storage)
        {
            _userID = userID;
            _storage = storage;
        }

        public void Execute()
        {
            User user = _storage.GetUser(_userID);
            if(user != null)
            {
                Console.WriteLine($"User: id = {user.userID}, name - {user.Name}, lastname - {user.lastName}");
            }
        }

        public void Undo()
        {
            throw new NotImplementedException();
        }
    }
}
