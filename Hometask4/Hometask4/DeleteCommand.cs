﻿using Hometask4.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hometask4
{
    class DeleteCommand : ICommand
    {
        private int _userID;
        private IStorage _storage;
        public DeleteCommand(int userID, IStorage storage)
        {
            _userID = userID;
            _storage = storage;
        }

        public void Execute()
        {
            _storage.Remove(_userID);   
        }

        public void Undo()
        {
            throw new NotImplementedException();
        }
    }
}
