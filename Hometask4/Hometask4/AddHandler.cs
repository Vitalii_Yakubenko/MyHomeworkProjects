﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hometask4.Interfaces;

namespace Hometask4
{
    class AddHandler : Handler
    {
        public override void HandlerRequest(string command, string[] obj)
        {
            if (command == "add")
            {
                ICommand com = new AddCommand(obj, StorageProvider.Storage);
                com.Execute();
                Console.WriteLine("User has been added.");
            }
            else if(Successor != null)
            {
                Successor.HandlerRequest(command, obj);
            }
        }

        public AddHandler(StorageProvider storageProvider) : base(storageProvider)
        {
        }
    }
}
