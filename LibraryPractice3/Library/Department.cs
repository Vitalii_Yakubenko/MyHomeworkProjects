﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

namespace Library
{
    internal class Department : CountingBooksBase
    {
        private readonly List<BookContainer> _books;

        [JsonProperty(PropertyName = "DepartmentName")]
        public string Name { get; set; }
        
        [JsonProperty(PropertyName = "Books")]
        public IReadOnlyList<BookContainer> Books
        {
            get { return _books.AsReadOnly(); }
            protected set
            {
                _books.Clear();
                foreach (var book in value)
                    AddBook(book.Book);
            }
        }

        public Department() : this("Unknown") { }

        public Department(string name)
        {
            Name = name;
            _books = new List<BookContainer>();
        }

        public void AddBook(Book book)
        { 
            var container = PeekContainer(book.Name);
            if (container != null)
                container.Count++;
            else
                _books.Add(new BookContainer{Book = book, Count = 1});
        }
        
        public Book RemoveBook(string name)
        {
            var container = PeekContainer(name);
            if (container == null) 
                return null;
            
            if (container.Count == 1)
                _books.Remove(container);
            else
                container.Count--;
            return container.Book;
        }
        
        public Book BookWithMinCount()
        {
            Book tmpBook = new Book("Non", 9999, new Author("Unknown"));
            if (_books != null)
            {
                foreach (var book in _books)
                {
                    if (book.Book.CompareTo(tmpBook) == -1)
                        tmpBook = book.Book;
                }
            }
            return tmpBook; 
        }

        private BookContainer PeekContainer(string name)
        {
            foreach (var container in _books)
                if (container.Book.Name == name)
                    return container;
            return null;
        }

        public override int BooksCount()
        {
            var count = 0;
            foreach (var container in _books)
                count += container.Count;
            return count;
        }

        public override string ToString()
        {
            return $"Name of department - {Name}, count of books - {BooksCount()}";
        }
    }
    
    internal class BookContainer
    {
        public Book Book { get; set; }
        public int Count { get; set; }
    }
}
