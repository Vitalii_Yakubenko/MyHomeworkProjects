﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Library
{
    class XmlLibrary
    {
        public static void WriteXml(Library input, string fileName)
        {
            XElement xLibrary = new XElement("Library");
            xLibrary.SetAttributeValue("name", input.Name);
            foreach (Department dep in input.Departments)
            {
                XElement xDepartment = new XElement("Department");
                xDepartment.SetAttributeValue("name", dep.Name);
                foreach (BookContainer book in dep.Books)
                {
                    XElement xBook = new XElement("Book");
                    xBook.SetAttributeValue("name", book.Book.Name);
                    xBook.SetAttributeValue("pages", book.Book.PagesCount);
                    xBook.SetAttributeValue("author", book.Book.Author.Name);
                    xDepartment.Add(xBook);
                }
                xLibrary.Add(xDepartment);
            }
            XDocument file = new XDocument(xLibrary);
            file.Save(fileName);
        }

        public static Library ReadXml(string fileName, out List<Author> authors)
        {
            XDocument xDoc = XDocument.Load(fileName);
            authors = new List<Author>();
            if (xDoc != null && xDoc.Root.HasElements)
            {
                XElement xLibrary = xDoc.Element("Library");
                Library result = new Library();
                result.Name = xLibrary.Attribute("name").Value;

                foreach (var xDepartment in xLibrary.Elements("Department"))
                {
                    Department currentDep = new Department();
                    currentDep.Name = xDepartment.Attribute("name").Value;
                    foreach (var xBook in xDepartment.Elements("Book"))
                    {
                        Book currentBook = new Book();
                        currentBook.Name = xBook.Attribute("name").Value;
                        currentBook.PagesCount = Int32.Parse(xBook.Attribute("pages").Value);
                        
                        Author currentAuthor = authors.Find(n => (n.Name == xBook.Attribute("author").Value));
                        if (currentAuthor == null)
                        {
                            currentAuthor = new Author(xBook.Attribute("author").Value);
                            authors.Add(currentAuthor);
                        }

                        currentBook.Author = currentAuthor;
                        currentAuthor.AddPublication(currentBook);
                        
                        currentDep.AddBook(currentBook);
                    }
                    result.AddDepartment(currentDep);
                }
                return result;
            }
            return null;
        }
        
        public static void Change(string fileName)
        {
            XDocument xDoc = XDocument.Load(fileName);
            if (xDoc != null && xDoc.Root.HasElements)
            {
                XElement xLibrary = xDoc.Element("Library");
                foreach (var xCategory in xLibrary.Elements("Department"))
                {
                    foreach (var xBook in xCategory.Elements("Book"))
                    {   
                        xBook.Attribute("name").SetValue(xBook.Attribute("name").Value = $"{xBook.Attribute("name").Value}Update");
                    }
                }
                xDoc.Save(fileName);
                return;
            }
        }
        public static void DeleteBook(string fileName, string bookName)
        {
            XDocument xDoc = XDocument.Load(fileName);
            if (xDoc != null && xDoc.Root.HasElements)
            {
                XElement xLibrary = xDoc.Element("Library");
                foreach (var xDepartment in xLibrary.Elements("Department"))
                {
                    foreach (var xBook in xDepartment.Elements("Book"))
                    {
                        if (xBook.Attribute("name").Value == bookName)
                        {
                            xBook.Remove();
                            xDoc.Save(fileName);
                            return;
                        }
                    }
                }
            }
        }
    }
}
