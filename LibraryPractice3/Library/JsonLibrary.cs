﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    class JsonLibrary
    {
        public static void WriteIntoJson(Library library)
        {
            var json = JsonConvert.SerializeObject(library);
            using (StreamWriter sw = new StreamWriter("LibraryJson.json"))
            {
                sw.WriteLine(json);
            }
        } 

        public static Library ReadFromJson(out List<Author> authorList)
        {
            Library library;
            authorList = new List<Author>();
            using (StreamReader sr = new StreamReader("LibraryJson.json"))
            {
                library = JsonConvert.DeserializeObject<Library>(sr.ReadLine());
            }
            foreach (var dep in library.Departments)
            {
                foreach (var book in dep.Books)
                {
                    if (book.Book != null)
                    {
                        book.Book.Author.AddPublication(book.Book);
                        Author curAuthor = authorList.FirstOrDefault(n => n.Name == book.Book.Author.Name);
                        if (curAuthor == null)
                        {
                            authorList.Add(book.Book.Author);
                        }
                    }
                }
            }
            return library;
        }
    }
}
