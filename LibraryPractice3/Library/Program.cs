﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

namespace Library
{
    internal class Program
    {
        public static List<Author> authorList;
        public static Library library;
        private static void Main(string[] args)
        {
            authorList = new List<Author> {new Author("Vitalii"), new Author("Taras"), new Author("Tania") };
            library = new Library("MyBookLibrary");

            Department IT = new Department("IT department");
            Department Foreigh = new Department("Foreigh literature department");
            Department Ukrainian = new Department("Ukrainian litareure department");
            Department Science = new Department("Ukrainian litareure department");
            library.AddDepartment(IT);
            library.AddDepartment(Foreigh);
            library.AddDepartment(Ukrainian);
            
            // Сheck the error
            library.AddDepartment(Science);
            
            Book spartak = new Book("Spartak", 300, (authorList.First(author => author.Name == "Vitalii")));
            Book fox = new Book("Fox", 100, (authorList.First(author => author.Name == "Tania")));
            Book witcher = new Book("Witcher", 1000, (authorList.First(author => author.Name == "Taras")));
            
            authorList.First(n => n.Name == "Vitalii").AddPublication(spartak);
            authorList.First(n => n.Name == "Tania").AddPublication(fox);
            authorList.First(n=> n.Name == "Taras").AddPublication(witcher);

            Ukrainian.AddBook(fox);
            Foreigh.AddBook(spartak);
            Foreigh.AddBook(witcher);

            Author newAuthor = new Author("Oleg");
            authorList.Add(newAuthor);

            // Work with Json serializer
            // Write into file
            JsonLibrary.WriteIntoJson(library);

            // Read from file
            //library = JsonLibrary.ReadFromJson(out authorList);
            /*Book test = new Book("Ant", 10, new Author("Taras"));
            var list = library.Departments.First(n => n.Name == "Foreigh literature department");
            list.AddBook(test);
            authorList.First(n => n.Name == test.Author.Name).AddPublication(test);*/


            // Work with XML serializer
            //XmlLibrary.WriteXml(library, "HomeWorkXMLLibrary.xml");
            XmlLibrary.Change("HomeWorkXMLLibrary.xml");
            library = XmlLibrary.ReadXml("HomeWorkXMLLibrary.xml", out authorList);

            // Тестові виводи на екран
            Console.WriteLine($"The book with min count of page in library - {library.BookWithMinCount()}");
            Console.WriteLine('\n' + $"The author with max count of books - {library.authorWithMaxBookCount(authorList).Name}");
            Console.WriteLine('\n' + $"The department with max count of books - {library.DepartmentWithMaxBooksNum().Name}");
            Console.ReadKey();
        }
        
    }
}
