﻿using Hometask5.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hometask5
{
    public partial class Form1 : Form, IView
    {
        private IFileManager _fileManager;
        private Presenter _presenter = null;
        private  IModel _model;
        public Form1 (IModel model, IFileManager fileManager)
        {
            this._model = model;
            _presenter = new Presenter(this, _model, fileManager);
            
            _fileManager = fileManager;
            _fileManager.path = Settings.getInstance();

            History = new List<string>();

            InitializeComponent();

        }
        public string UserName
        {
            get { return txtbName.Text; }
            set { }
        }

        public string LastName
        {
            get
            {
                return txtbLastName.Text;
            }

            set { }
        }

        public int Age
        {
            get
            {
                return Int32.Parse(txtbAge.Text);
            }

            set { }
        }

        public string WorkPlace
        {
            get
            {
                return txtbWorkPlace.Text;
            }

            set { }
        }

        public string FilePath
        {
            get
            {
                return txtbPath.Text;
            }

            set {  }
        }      
        public List<String> History { get; set; }

        public event EventHandler<EventArgs> AddUser;
        public event EventHandler<EventArgs> GetLastUser;

        private string SetPath()
        {
            string path = (string)_fileManager.ReadFromFile();
            
            if (path != null)
            {
                return path;
            }
            else return String.Empty;
        }
        private void addBtn_Click(object sender, EventArgs e)
        {
            if (this.AddUser != null)
                this.AddUser(this, EventArgs.Empty);
           
            richTextBox1.Text +=History.Last();

            //When you first time choose your DB file to save path in settings
            if (SetPath() == string.Empty)
            {
                _fileManager.WriteToFile(FilePath);
            }
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            if (this.GetLastUser != null)
                this.GetLastUser(this, EventArgs.Empty);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                txtbPath.Text = openFileDialog1.FileName;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            txtbPath.Text = SetPath();
        }
    }
}
