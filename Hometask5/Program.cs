﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Hometask5.Interfaces;
using Microsoft.Practices.Unity;

namespace Hometask5
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var container = new UnityContainer();
            container.RegisterType<IFileManager, FileManager>();
            container.RegisterType<IModel, Model>();
            container.RegisterType<ICSVserializer, CSVserializer>();
            
            var Form = container.Resolve<Form1>();
            Application.Run(Form);
        }
    }
}
