﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;
using System.Windows.Forms;
using Hometask5.Interfaces;

namespace Hometask5
{
    class Presenter
    {
        private IFileManager _fileManager;
        private IView _view;
        private IModel _model;
       
        public Presenter(IView view, IModel model, IFileManager fileManager)
        { 
            this._model = model;
            this._view = view;
            this._fileManager = fileManager;
            Initialize();
        }
        private void Initialize()
        {
            _view.AddUser += Add;
            _view.GetLastUser += GetLast;
            InitialText();
        }
        private void InitialText()
        {
            _view.UserName = null;
            _view.LastName = null;
            _view.Age = 0;
            _view.WorkPlace = null;
        }
        private void Add(object sender, EventArgs e)
        {            
            if (_view.FilePath != null)
            {
                _model.SetPathToFile(_view.FilePath);
                User user = new User
                {
                    Name = _view.UserName,
                    LastName = _view.LastName,
                    Age = _view.Age,
                    WorkPlace = _view.WorkPlace
                };
                _model.Set(user);
                MessageBox.Show("User is added successfully");

                AddHistory(user);
            }
            else
            {
                MessageBox.Show("Your file path is empty. Please enter it");
            }
        }
        private void AddHistory(User user)
        {
            string str = DateTime.Now.ToString() + " was added " + user.Name + " " + user.LastName + "\n";
            _view.History.Add(str);
        }
      
        private void GetLast(object sender, EventArgs e)
        {
            _model.SetPathToFile(_view.FilePath);
            User user = (User)_model.Get().Last();
            MessageBox.Show("The last user in your Db is " + user.Name + "  " + user.LastName );
        }
    }
}
