﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hometask5.Interfaces
{
    public interface ICSVserializer
    {
        StringBuilder Serialize(List<User> users);
        List<User> Deserialize(StringBuilder document);
    }
}
