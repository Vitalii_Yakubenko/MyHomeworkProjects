﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hometask5
{
    interface IView
    {
        string UserName { get; set; }
        string LastName { get; set; }
        int Age { get; set; }
        string WorkPlace { get; set; }

        string FilePath { get; set; }
        List<string> History { get; set; }

        event EventHandler<EventArgs> AddUser;      
        event EventHandler<EventArgs> GetLastUser;

    }
}
