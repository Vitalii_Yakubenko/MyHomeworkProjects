﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hometask5
{
   public interface IModel
    {
        List<User> Get();
        void Set(User user);
        void SetPathToFile(string path);
    }
}
