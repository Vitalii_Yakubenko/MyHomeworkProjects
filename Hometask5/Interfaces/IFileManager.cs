﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hometask5.Interfaces
{
    public interface IFileManager
    {
        string path { get; set; }
        void WriteToFile(StringBuilder data);
        void WriteToFile(string data);
        StringBuilder Read();
        string ReadFromFile();
    }
}
