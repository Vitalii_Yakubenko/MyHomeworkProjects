﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hometask5.Interfaces;

namespace Hometask5
{
    class CSVserializer : ICSVserializer
    {
        public CSVserializer() { }

        public StringBuilder Serialize(List<User> users)
        {
            StringBuilder csv = new StringBuilder();
            foreach (var user in users)
            {
                csv.AppendFormat("{0},{1},{2},{3}\n", user.Name, user.LastName, user.Age, user.WorkPlace);
            }

            return csv;
        }
        public List<User> Deserialize(StringBuilder document)
        {
            List<User> Users = new List<User>();
            string[] lines = document.ToString().Split('\n');
            for (int i = 0; i < lines.Length; i++)
            {
                List<string> parametrs = lines[i].Split(',').ToList();

                if (parametrs.Count() == 4)
                {
                    Users.Add(new User
                    {
                        Name = parametrs[0],
                        LastName = parametrs[1],    
                        Age = Int32.Parse(parametrs[2]),
                        WorkPlace = parametrs[3]
                    });
                }
                parametrs.Clear();
            }
            return Users;
        }
    }
}
