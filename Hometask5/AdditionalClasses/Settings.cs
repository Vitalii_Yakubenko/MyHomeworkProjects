﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Hometask5
{
    class Settings
    {
        private static string Path;

        public Settings() { }
     
        public static string getInstance()
        {
            if (Path == null)
            {
                if (!File.Exists("Settings.txt"))
                {
                    File.CreateText("Settings.txt");
                }
                Path = "Settings.txt";
            }
            return Path;
        }
    }
}
