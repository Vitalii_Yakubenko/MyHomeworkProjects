﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Hometask5.Interfaces;

namespace Hometask5
{
    class FileManager : IFileManager
    {
        
        public string path { get; set; }
        
        public FileManager() { }

        public void WriteToFile(StringBuilder data)
        {
            using (StreamWriter sw = new StreamWriter(path, false))
            {
                sw.WriteLine(data);
            }  
        }
        public void WriteToFile(string data)
        {
            using (StreamWriter sw = new StreamWriter(path, false))
            {
                sw.WriteLine(data);
            }
        }
        public StringBuilder Read()
        {
            StringBuilder document = new StringBuilder();
            using (StreamReader sr = new StreamReader(path))
            {
                document.Append(sr.ReadToEnd());
            }
            return document;
        }
        public string ReadFromFile()
        {
            string document;
                      
            using (StreamReader sr = new StreamReader(path))
            {
                document = sr.ReadToEnd();
            }
            return document;
        }
    }
}
