﻿using Hometask5.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hometask5
{
    class Model : IModel
    {
        private IFileManager _fileManager;
        private ICSVserializer _serializer;
        public List<User> Users { get; set; }
        public Model(IFileManager fileManager, ICSVserializer serializar)
        {
            Users = new List<User>();
            _fileManager = fileManager;
            _serializer = serializar;
        }
        public void SetPathToFile(string path)
        {
            _fileManager.path = path;
        }
        public void Set(User user)
        {
            Users.Add(user);
            _fileManager.WriteToFile(_serializer.Serialize(Users));
        }
        public List<User> Get()
        {
            _fileManager.Read();
            return _serializer.Deserialize(_fileManager.Read());
        }
    }
}
